#! /usr/bin/env/ python3

from setuptools import setup, find_packages

setup(name="technocore",
      version="0.1.12",
      description="technocore application. A command line contact book.",
      author="Thomas Rostrup Andersen",
      author_email="andersen1814@gmail.com",
      license="BSD 2-clause",
      keywords="contact book manager technocore cli tui",
      packages=find_packages(),
      entry_points={'console_scripts':
            ['technocore=technocore.app:main']},
      install_requires=["npyscreen", "vobject"],
      include_package_data=True,
      data_files=[('technocore/resources/licenses/', ['technocore/resources/licenses/technocore_license.md'])]
      )

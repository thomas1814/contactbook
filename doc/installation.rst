.. Document the installation.

Installation
############
The technocore contactbook can be installed using pip:

.. code-block:: bash

   $ pip install technocore --user

To start the application, open a terminal and enter:

.. code-block:: bash

    $ technocore

If you receive an error about technocore command not found, you will most likely have to add it to your PATH vaiable. This will depend on you system, but typically the command is located under "/Python/bin/.". Once added to PATH, relaunch the terminal.

To uninstall from pip::

    pip uninstall technocore

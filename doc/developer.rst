.. Document the development environment and tools for developers.

Developer
#########

All contributions (pull/merge requests) are welcome.


Source Code:
------------
The source code is available at gitlab::

    git clone git@gitlab.com:thomas1814/contactbook.git

The application has some requirements:

* Python 3.6 - Application is written in Python.
* pip (9.0.1): Comes with Python: :code:`$ pip install --upgrade pip`
* npyscreen 4.10.5: :code:`$ pip install npyscreen` - Python library for terminal user interface.
* vobject (0.9.5): :code:`$ pip install vobject` - Python library for import/exporting contacts in the vCard format.

The source code can be run using::

    python technocore.py


Building and Installing the Application Locally from Source Code
----------------------------------------------------------------
Requirements:

* setuptools (38.4.0): :code:`$ pip install setuptools --user`
* wheel (0.30.0): :code:`$ pip install wheel --user`

The application is installed using a wheel package. A wheel package can be built using Python`s setuptool and wheel tool. In the project root folder::

    python setup.py bdist_wheel clean

The wheel package can be installed locally using setup and pip::

    python setup.py bdist_wheel clean
    cd dist/
    pip install technocore-0.1.*-py3-none-any.whl --user

To start the application, open a terminal and enter::

    technocore

If you receive an error about technocore command not found, you will most likely have to add it to your PATH variable. This will depend on you system, but typically the command is located under "/Python/bin/.". Once added to PATH, relaunch the terminal.

To uninstall from pip::

    pip uninstall technocore


Publish a New Version to PyPi
-----------------------------
The application is distributed using pip, Python's default package manager. Pip often comes with Python itself and can be accessed from the terminal with the pip command::

    $ pip

To publish to pip, the following tools (requirements) are needed:

* pip (9.0.1): Comes with Python: :code:`$ pip install --upgrade pip`
* setuptools (38.4.0): :code:`$ pip install setuptools --user`
* wheel (0.30.0): :code:`$ pip install wheel --user`
* twine (1.9.1): :code:`$ pip install twine --user`

The application is distributed using the a wheel package. A wheel package can be built using Python`s setuptool and wheel tool. In the project root folder::

    python setup.py bdist_wheel clean

The wheel package can be uploaded to pip by using twine::

    twine upload dist/*

You will have to have a pip account with access to the technocore name space to upload the application.


Writing Documentation
---------------------
The documentation for the application is located in the doc folder and is written in English in the reStructuredText (rst) format. Sphinx is used for converting the documentation into html. The html files is automatically published using readthedocs and can be accessed at::

    https://readthedocs.org/projects/contactbook/

Requirements:

* Sphinx (1.6.6): :code:`$ pip install sphinx --user` - Python documentation generator.
* sphinx-autobuild (0.7.1): :code:`$ pip install sphinx-autobuild --user` - automaticaly build html files when rst files changes.
* sphinx-rtd-theme (0.2.4): :code:`$ pip install sphinx-rtd-theme --user` - The html theme used at the online website.

If you want to write documentation, simply add/edit the rst files in the doc folder using the reStructuredText format. Then build the html files using (in the doc directory)::

    sphinx-build -b html . ./_build

The html files can be viewed by opening the index.html file in the _build folder. If you want to automatically build changes saved to the files, use::

    sphinx-autobuild . _build/html

The changes are automatically imported to readthedocs when changes are committed to the application repository`s master branch at Gitlab.

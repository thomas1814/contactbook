.. Document the generic application commands.

Application Commands
####################
This section contains general application command and usage, for commands related to specific features see the features respective sections.

About
-----
The about screen can be opened using the about command::

    :about

Clear Screen
------------
The screen can be cleared by using the clear command::

    :clear

License
-------
The license can be viewed by using the license command::

    :license

Back
----
To go back, if possible, use the back command::

    :back

Quit Application
----------------
To quit the application, use the quit command::

    :quit

.. Document how to use the contactbook.

Contact Book
############

To start the contact book from the home screen use the contacts command::

    :contacts

The main part of the contact book has a mutt like interface. It has a upper status line, telling how many contacts are stored in the database. The main widget that display contacts. The lower status line, displaying information related to the current view or last command. The command line. The command line can be entered with the search key "/" or the command key ":".

Contact Information
-------------------
The data is stored in a database. The database is documented in databasemodel.dia in the doc folder in the source code repository. A contact can have the following information:

* First name
* Last name
* Title
* Birthday
* Death
* Sex
* Emails
* Phone numbers
* Addresses
* Groups
* Organisations

Manage contacts
---------------
* Add a new contact: ctrl+a.
* Edit contact: When selected use ENTER or ctrl+e.
* Delete contact: When delected ctrl+d.

When a new contact is created or one is selected for editing, the application will open up a form for easy editing. The form can be navigated using the tab key and arrows.

List All Contacts
-----------------
To list all contacts, use the list command::

    :list

Search
------
To search for one or several contacts, use the search command::

    /john doe 555

The results will update live as you are typing. It searches for all contacts that has data relevant to the search term. Each word is considered a new search term, but only contacts that matches all search terms are displayed.

Display a group of contacts
---------------------------
Contacts who are part of a group can be displayed using the group command::

    :group <name of group>

The group command updates live and searches for contacts that belongs to a group that is relevant to the search term.

Filter
------
To filter contacts, use the filter command::

    :filter

WARNING: Not fully implemented.

Import
------
Contacts can be imported from a .vcf file that contains contact information stored in vCard format. To import from a file use the import command::

    :import /full/path/to/filename.vcf

Export
------
Contacts can be exported to a .vcf file in the vCard format. To export the currently displayed contacts, use the export command::

    :export /full/path/to/filename.vcf

WARNING: If a file already exist, it will be overwritten without warning.

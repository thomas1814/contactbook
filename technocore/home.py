#!/usr/bin/env python3
import npyscreen
from .database import Database, Options
from .applicationdatabase import AppDB

__author__ = "Thomas Rostrup Andersen"
__copyright__ = """Copyright (C) 2017 Thomas Rostrup Andersen.
        All rights reserved."""
__license__ = "BSD 2-clause"
__version__ = "0.1.8"
__all__ = ["HomeForm"]


class HomeListWidget(npyscreen.MultiLineAction):
    def __init__(self, *args, **keywords):
        super().__init__(*args, **keywords)
        self.add_handlers({
                          # "y": self.yes,
                          })
        self.filter = None
        self.query = None
        self.values = []
        self.interactive = True
        self.displaying = None
        # self.hidden = True

    def yes(self, event):
        self.clear_screen()

    def show_about(self):
        self.displaying = "about"
        self.interactive = False
        self.values = ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "                                Version 0.1.8", "                   Copyright 2017 Thomas Rostrup Andersen", "                            License: BSD 2-clause"]
        self.display()

    def clear_screen(self):
        self.interactive = True
        self.values = []
        self.filter = None
        self.query = None
        self.displaying = None
        self.display()

    def display_value(self, vl):
        if self.displaying == "themes":
            theme = Database.get_setting(key="theme")[0]
            if theme.value == vl:
                return "  * " + vl + "    "
            else:
                return "    " + vl + "    "
        if type(vl).__name__ == "str":
            if vl == "":
                return vl
            if " " in vl[0]:
                return vl
            else:
                return "    " + vl + "    "

    def update_list(self):
        if self.filter is not None:
            self.filter(self.query)
        else:
            self.values = []
        self.display()

    def h_cursor_line_down(self, ch):
        if self.interactive:
            super().h_cursor_line_down(ch)

    def show_themes(self):
        self.interactive = True
        self.displaying = "themes"
        self.values = Options.THEMES
        self.display()

    def actionHighlighted(self, act_on_this, keypress):
        if self.displaying == "themes":
            if act_on_this in Options.THEMES:
                Database.update_setting(key="theme", value=act_on_this)
                self.parent.wStatus2.value = " Status - Restart application to change theme... "
            else:
                self.parent.wStatus2.value = " Status - Theme does not exist... "
            self.parent.display()


class HomeActionController(npyscreen.ActionControllerSimple):

    def create(self):
        self.add_action("^/.*", self.search, True)
        self.add_action("^:contacts.*", self.open_contacts, False)
        self.add_action("^:setting theme list", self.setting_theme_list, False)
        self.add_action("^:setting theme set .*", self.setting_theme, False)
        self.add_action("^:quit", self.quit, False)
        self.add_action("^:about", self.about, False)
        self.add_action("^:clear", self.clear, False)
        self.add_action("^:license", self.license, False)
        # self.add_action("^y", self.clear, False)

    def license(self, command_line, widget_proxy, live):
        self.parent.wMain.interactive = False
        licenses = AppDB.get_all_license()
        # self.parent.wMain.values.append(licenses[0].description)
        # self.parent.display()
        import textwrap
        text_terms = licenses[0].description
        parts = text_terms.split("\n")
        text = [""]
        for part in parts:
            if part == "\n" or part == "":
                text.append("\n")
            else:
                wrap = textwrap.wrap(part, width=65)
                for line in wrap:
                    text.append(line)
        self.parent.wMain.values = text
        self.parent.wMain.display()

        self.parent.wStatus1.value = " technocore - License "
        self.parent.wStatus2.value = " Status - Do you agree [yes/no]?"
        self.parent.display()

    def quit(self, command_line, widget_proxy, live):
        self.parent.parentApp.switchForm(None)

    def about(self, command_line, widget_proxy, live):
        self.parent.wStatus1.value = " technocore - About "
        self.parent.wMain.show_about()
        self.parent.wStatus2.value = " Status "
        self.parent.display()

    def clear(self, command_line, widget_proxy, live):
        self.parent.wStatus1.value = " technocore "
        self.parent.wMain.clear_screen()
        self.parent.wStatus2.value = " Status "
        self.parent.display()

    def search(self, command_line, widget_proxy, live):
        pass

    def open_contacts(self, command_line, widget_proxy, live):
        self.parent.parentApp.switchForm("CONTACT_BOOK")

    def setting_theme_list(self, command_line, widget_proxy, live):
        self.parent.wStatus1.value = " technocore - Setting "
        self.parent.wMain.show_themes()
        self.parent.wStatus2.value = " Status - Displaying themes... "
        self.parent.display()

    def setting_theme(self, command_line, widget_proxy, live):
        self.parent.wStatus1.value = " technocore "
        self.parent.wStatus2.value = " Status - Changeing theme... "
        self.parent.display()
        cmd = ":setting theme set "
        index = command_line.find(cmd)
        theme = command_line[index + len(cmd):]
        if theme in Options.THEMES:
            Database.update_setting(key="theme", value=theme)
            self.parent.wStatus2.value = " Status - Restart application to change theme... "
        else:
            self.parent.wStatus2.value = " Status - Theme does not exist... "
        self.parent.display()


class HomeForm(npyscreen.FormMuttActiveTraditionalWithMenus):
    MAIN_WIDGET_CLASS = HomeListWidget
    ACTION_CONTROLLER = HomeActionController
    MAIN_WIDGET_CLASS_START_LINE = 2

    def init(self):
        super().__init__()

    def create(self):
        super().create()
        self.wStatus1.value = " technocore "
        self.wStatus2.value = " Status "

        self.add_handlers({
                          "y": self.wMain.yes,
                          })
        del self.handlers["y"]
        # self.remove_event_handler("y", self.wMain.yes,)
